## Bateau Thibault Front

### Prérequis

1- Clonez le repository : `git clone https://gitlab.com/bateau-thibault/bateau-thibault-front.git`

2- Mettez vous sur la branch develop `git checkout develop` 

3- Lancer la commande : `npm install` 



## Démarrage du projet

1- Lancer `ng serve` pour démarrer le projet 
