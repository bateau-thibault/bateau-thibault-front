import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'my-login-form',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
  loginForm: FormGroup;
  errorMessage: string | undefined;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]], 
    });
  }

  login() {
    if (this.loginForm.valid) {
      const { username, password } = this.loginForm.value;
      const body = JSON.stringify({ username: username, password: password });
      const headers = { 'Content-Type': 'application/json' };
      this.http.post<any>('http://127.0.0.1:8000/api/token/', body, { headers }).subscribe({
        next: data => {
          if (data && data.access) {
            localStorage.setItem('access_token', data.access);
            localStorage.setItem('refresh_token', data.refresh);

            this.router.navigate(['/home']);
          } else {
            
            this.errorMessage = 'Identifiants incorrects. Veuillez réessayer.';
          }
        },
        error: error => {
          {
            console.error('There was an error!', error);
          }

        }
      })
    }

  }

  @Input() error: string | null = '';

  @Output() submitEM = new EventEmitter();
}
