import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'signup-root',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent {
  signupForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient
  ) {
    this.signupForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      role: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.signupForm.valid) {
      const { username, email, password, role } = this.signupForm.value;
      // Send POST request to the API
      const headers = { 'Content-Type': 'application/json' };

      const body = JSON.stringify({
        username: username,
        email: email,
        password: password,
        role: role,
      });

      this.http
        .post<any>('http://127.0.0.1:8000/createuser/', body, { headers })
        .subscribe({
          next: (data) => {
            if (data && data.message === 'Vous êtes inscrit') {
              this.router.navigate(['/login']);
            } else {
              console.error('Connexion échouée');
            }
          },
          error: (error) => {
            console.error('There was an error!', error);
          },
        });
    }
  }
}
