import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './features/header/header.component';
import { DetailsProduitsComponent } from './pages/details-produits/details-produits.component';
import { FooterComponent } from './features/footer/footer.component';
import { ProductsService } from './core/services/products.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { StockValidatorDirective } from './core/services/stock-validator.directive';
import { SignupModule } from './signup/signup.module';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardModule } from './dashboard/dashboard.module';
import { StockProduitComponent } from './stock/stock-produit.component';
import { AuthService } from './core/services/auth.service';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    DetailsProduitsComponent,
    FooterComponent,
    StockValidatorDirective,
    StockProduitComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    SignupModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    DashboardModule,
  ],
  providers: [ProductsService, AuthService],
  bootstrap: [AppComponent],
})
export class AppModule {}
