import { Component, OnInit } from '@angular/core';
import { StatsService } from '../core/services/dashboard.service';
import { StockProduitApi } from '../core/services/products.service';
import { AuthService } from '../core/services/auth.service';

@Component({
  selector: 'app-stock-produit',
  templateUrl: './stock-produit.component.html',
  styleUrls: ['./stock-produit.component.css'],
})
export class StockProduitComponent implements OnInit {
  stocks: StockProduitApi[] = [];

  constructor(private statsService: StatsService, private authService:AuthService) {}

  async ngOnInit() {

    var isTrue = await this.authService.checkIfTokenValid();
    if(isTrue)
    {
      await this.getStats();
    }
  }

  async getStats() {
    try {
      const stocksRes: StockProduitApi[] | undefined = await this.statsService
        .getStatsFromJson()
        .toPromise();
      if (stocksRes !== undefined) {
        this.stocks = stocksRes;
      } else {
        console.error('Les données JSON sont undefined.');
      }
    } catch (err) {
      console.error('Erreur lors du chargement des données JSON :', err);
    }
  }
}
