import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js/auto';
import {
  IProductApi,
  Product,
  ProductsService,
} from '../core/services/products.service';
import { StatsService } from '../core/services/dashboard.service';
import { AuthService } from '../core/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  listeProduits: Product[] = [];
  poissons: Product[] = [];
  fruitsDeMer: Product[] = [];
  crustaces: Product[] = [];
  chiffreDaffaire: number = 0;

  moisListe: string[] = [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre',
  ];

  anneeListe: number[] = Array.from(
    new Set(
      // @ts-ignore
      this.listeProduits.map((p) => new Date(p.created_date).getFullYear())
    )
  ).sort();

  private _periode: string = 'Année';
  get periode(): string {
    return this._periode;
  }
  set periode(val: string) {
    this._periode = val;
    this.updateChiffreDaffaireAndChart();
  }

  private _mois: number = 1;
  get mois(): number {
    return this._mois;
  }
  set mois(val: number) {
    this._mois = val;
    this.updateChiffreDaffaireAndChart();
  }

  private _annee: number = 2023;
  get annee(): number {
    return this._annee;
  }
  set annee(val: number) {
    this._annee = val;
    this.updateChiffreDaffaireAndChart();
  }

  updateChiffreDaffaireAndChart(): void {
    this.afficherChiffreDaffaire();
    this.refreshChart();
  }

  refreshChart(): void {
    if (this.lineChart) {
      this.lineChart.destroy();
    }
    this.createChart();
  }

  getSoldProductsCount(category: string): number {
    // @ts-ignore
    const products = this.regrouperProduitsParCategorie(this.listeProduits)[
      category
    ];
    return products ? products.filter((p) => p.sale).length : 0;
  }

  getStockCount(category: string): number {
    // @ts-ignore
    const products = this.regrouperProduitsParCategorie(this.listeProduits)[
      category
    ];
    return products
      ? products.reduce((acc, p) => acc + p.quantityInStock, 0)
      : 0;
  }

  calculerChiffreDaffaireByCategoryFor(category: string): number {
    // @ts-ignore
    const chiffres = this.calculerChiffreDaffaireByCategory(this.listeProduits);
    return chiffres[category] || 0;
  }

  afficherChiffreDaffaire(): void {
    let debutDate: Date;
    let finDate: Date;

    switch (this.periode) {
      case 'Mois':
        debutDate = new Date(this.annee, this.mois, 1);
        finDate = new Date(this.annee, this.mois + 1, 0);
        break;
      case 'Trimestriel':
        debutDate = new Date(this.annee, this.mois, 1);
        finDate = new Date(this.annee, this.mois + 3, 0);
        break;
      case 'Année':
        debutDate = new Date(this.annee, 0, 1);
        finDate = new Date(this.annee, 11, 31);
        break;
      default:
        return;
    }

    this.chiffreDaffaire = this.listeProduits
      .filter(
        (p) =>
          // @ts-ignore
          new Date(p.created_date) >= debutDate &&
          // @ts-ignore
          new Date(p.created_date) <= finDate
      )
      .reduce((total, p) => total + p.price * p.quantityInStock, 0);
  }

  produit = {
    stock: '',
  };

  lineChart: any;
  dataApi: any[] = [];
  months: string[] = [];
  public chart: any;

  // Jeu de données pour test
  sampleData: any[] = Array.from({ length: 120 }, (_, i) => ({
    created: new Date(2022 + Math.floor(i / 10), (i % 10) + 1, 1),
    tig_id: i,
    name: 'Produit ' + i,
    category: Math.floor(Math.random() * 10) + 1,
    price: Math.floor(Math.random() * (300 - 20 + 1) + 20),
    unit: 'unité',
    availability: Math.random() < 0.5,
    sale: Math.random() < 0.5,
    discount: Math.floor(Math.random() * 10),
    comments: 'Commentaire pour produit ' + i,
    owner: 'tig_orig',
    quantityInStock: Math.floor(Math.random() * 100),
  }));

  constructor(
    private productsService: ProductsService,
    private authService : AuthService
  ) {}

  async ngOnInit() {

    var isTrue = await this.authService.checkIfTokenValid();
    if(isTrue)
    {
      await this.getStats();
      this.initAnneeListe();
      this.createChart();
    }
  }

  initAnneeListe() {
    this.anneeListe = Array.from(
      new Set(
        // @ts-ignore
        this.listeProduits.map((p) => new Date(p.created_date).getFullYear())
      )
    ).sort();
  }

  async getStats() {
    try {
      const statsRes: Product[] | undefined = await this.productsService
        .getProductsFromJson()
        .toPromise();
      if (statsRes !== undefined) {
        this.listeProduits = statsRes;
        // this.createChart();
      } else {
        console.error('Les données JSON sont undefined.');
      }
    } catch (err) {
      console.error('Erreur lors du chargement des données JSON :', err);
    }
  }

  getColorByCategory(category: string): string {
    switch (category) {
      case 'Poissons':
        return '#184e77';
      case 'Fruits de Mer':
        return '#1e6091';
      case 'Crustacés':
        return '#1a759f';
      default:
        return '';
    }
  }

  regrouperProduitsParCategorie(produits: IProductApi[]) {
    const produitsRegroupes: { [key: string]: any[] } = {};
    const categoriesMapping = {
      0: 'Poissons',
      1: 'Fruits de Mer',
      2: 'Crustacés',
    };
    for (const produit of produits) {
      // @ts-ignore
      const categorieNom = categoriesMapping[produit.category];
      if (!produitsRegroupes[categorieNom]) {
        produitsRegroupes[categorieNom] = [];
      }
      produitsRegroupes[categorieNom].push(produit);
    }

    return produitsRegroupes;
  }

  fetchData() {
    // Remplacez par une API pour récupérer les vraies données
    this.dataApi = this.sampleData;
  }

  filterData(month: string) {
    const [selectedMonth, selectedYear] = month.split('-').map(Number);
    this.dataApi = this.sampleData.filter((item) => {
      const date = new Date(item.created);
      return (
        date.getMonth() + 1 === selectedMonth &&
        date.getFullYear() === selectedYear
      );
    });
    // this.initChart();
  }

  groupByMonthAndYear(): { [key: string]: number } {
    // @ts-ignore
    let data: IProductApi[] = this.listeProduits;
    const result: { [key: string]: number } = {};

    for (const item of data) {
      const date = new Date(item.created_date);
      const key = `${date.getMonth() + 1}-${date.getFullYear()}`;
      if (!result[key]) {
        result[key] = 0;
      }
      result[key] += item.price;
    }

    return result;
  }

  convertirDateEnMois(date: string): string {
    const mois = [
      'Janvier',
      'Février',
      'Mars',
      'Avril',
      'Mai',
      'Juin',
      'Juillet',
      'Août',
      'Septembre',
      'Octobre',
      'Novembre',
      'Décembre',
    ];
    const indexMois = new Date(date).getMonth();
    return mois[indexMois];
  }

  filtrerProduitsParPeriode(): Product[] {
    let debutDate: Date;
    let finDate: Date;

    switch (this.periode) {
      case 'Mois':
        debutDate = new Date(this.annee, this.mois, 1);
        finDate = new Date(this.annee, this.mois + 1, 0);
        break;
      case 'Trimestriel':
        debutDate = new Date(this.annee, this.mois, 1);
        finDate = new Date(this.annee, this.mois + 3, 0);
        break;
      case 'Année':
        debutDate = new Date(this.annee, 0, 1);
        finDate = new Date(this.annee, 11, 31);
        break;
      default:
        return [];
    }

    return this.listeProduits.filter((p) => {
      // @ts-ignore
      const productDate = new Date(p.created_date);
      return productDate >= debutDate && productDate <= finDate;
    });
  }

  calculerChiffreDaffaire(): { dates: string[]; chiffresDaffaire: number[] } {
    const produitsFiltres = this.filtrerProduitsParPeriode();
    const data = {
      dates: this.generateDatesBasedOnPeriod(),
      chiffresDaffaire: [],
    };

    for (const date of data.dates) {
      const totalForDate = produitsFiltres
        // @ts-ignore
        .filter((p) => this.convertirDateEnMois(p.created_date) === date)
        .reduce((total, p) => total + p.price * p.quantityInStock, 0);
      // @ts-ignore
      data.chiffresDaffaire.push(totalForDate);
    }

    return data;
  }

  calculerChiffreDaffaireByCategory(produits: IProductApi[]) {
    const produitsParCategorie = this.regrouperProduitsParCategorie(produits);
    const chiffreDaffaire: { [key: string]: number } = {};

    for (const categorie in produitsParCategorie) {
      chiffreDaffaire[categorie] = produitsParCategorie[categorie].reduce(
        (sum, produit) => {
          return sum + produit.price * produit.sold;
        },
        0
      );
    }

    return chiffreDaffaire;
  }

  generateDatesBasedOnPeriod(): string[] {
    let debutDate: Date;
    let finDate: Date;
    let dates: string[] = [];

    switch (this.periode) {
      case 'Mois':
        debutDate = new Date(this.annee, this.mois, 1);
        finDate = new Date(this.annee, this.mois + 1, 0);
        for (let d = debutDate; d <= finDate; d.setDate(d.getDate() + 1)) {
          dates.push(this.convertirDateEnMois(d.toString()));
        }
        break;

      case 'Trimestriel':
        debutDate = new Date(this.annee, this.mois, 1);
        finDate = new Date(this.annee, this.mois + 3, 0);
        for (let d = debutDate; d <= finDate; d.setMonth(d.getMonth() + 1)) {
          dates.push(this.convertirDateEnMois(d.toString()));
        }
        break;

      case 'Année':
        for (let m = 0; m < 12; m++) {
          dates.push(this.moisListe[m]);
        }
        break;

      default:
        return [];
    }

    return dates;
  }

  createChart() {
    const calculerChiffreDaffaire = this.calculerChiffreDaffaire();

    this.lineChart = new Chart('chiffreAffaires', {
      type: 'line',
      data: {
        labels: calculerChiffreDaffaire.dates,
        datasets: [
          {
            label: "Chiffre d'affaire",
            data: calculerChiffreDaffaire.chiffresDaffaire,
            backgroundColor: '#ad1fff',
          },
        ],
      },

      options: {
        responsive: true,
        plugins: {
          title: {
            display: false,
          },
        },
        scales: {
          y: {
            // the data minimum used for determining the ticks is Math.min(dataMin, suggestedMin)
            suggestedMin: 30,

            // the data maximum used for determining the ticks is Math.max(dataMax, suggestedMax)
            suggestedMax: 50,
          },
        },
      },
    });
    // this.afficherChiffreDaffaire(')
  }
}

const configDonut = {
  type: 'doughnut',
  options: {
    responsive: true,
    plugins: {},
  },
};
