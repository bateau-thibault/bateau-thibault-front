import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, switchMap } from 'rxjs/operators';
import { throwError, of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor(private http: HttpClient) {}
  
  private productsUrl = 'http://127.0.0.1:8000/';

  getProductsFromJson(): Observable<Product[]> {
    const access_token = localStorage.getItem('access_token');

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${access_token}`,
    });

    return this.http
      .get<Product[]>(this.productsUrl + 'infoproducts', { headers });
  }

  getProductsDashboardFromJson(): Observable<IProductApi[]> {
    const access_token = localStorage.getItem('access_token');

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${access_token}`,
    });

    // @ts-ignore
    return this.http
      .get<IProductApi[]>(this.productsUrl + 'infoproducts', { headers });
  }

  getStocksFromJson(): Observable<any[]> {

    const access_token = localStorage.getItem('access_token');

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${access_token}`,
    });

    return this.http
      .get<any[]>(this.productsUrl + 'transactions', { headers });
  }

  updateProductStockAndPurchasePrice(product: Product) {
    const headers = { 'Content-Type': 'application/json' };

    const body = JSON.stringify(product);

    this.http
      .post<any>(this.productsUrl + 'updatestock/', body, { headers })
      .subscribe({
        next: (data) => {
        },
        error: (error) => {
          console.error('There was an error!', error);
        },
      });

    this.getProductsFromJson();
  }

  updatedPourcentageProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(this.productsUrl, product);
  }
  updateProductStockList(produit: Product): Observable<Product> {
    const updateData = {
      id: produit.id,
      quantityInStock: produit.quantityInStock,
    };
    return this.http.put<Product>(
      `${this.productsUrl}/${produit.id}`,
      updateData
    );
  }
  updateProductPriceAndStock(produit: Product): Observable<Product> {
    const updateData = {
      id: produit.id,
      achat_prix: produit.achat_prix,
      quantityInStock: produit.quantityInStock,
    };
    return this.http.put<Product>(
      `${this.productsUrl}/${produit.id}`,
      updateData
    );
  }
}

export interface Product {
  id: number;
  unit: string;
  category: number;
  name: string;
  discount: number;
  comments: string;
  owner: string;
  price: number;
  price_on_sale: number;
  sale: boolean;
  availability: boolean;
  quantityInStock: number;
  quantity_sold: number;
  modifiedStock: number;
  modifiePourcentage: number;
  achat_prix: number;
}

export interface IProductApi {
  availability: boolean;
  category: number;
  comments: string;
  created_date: string;
  discount: number;
  id: number;
  name: string;
  owner: string;
  price: number;
  quantityInStock: number;
  quantity_sold: number;
  sale: boolean;
  tig_id: number;
  unit: string;
}


export interface StockProduitApi {
  id: number;
  product_id: number;
  product_stock: number;
  product_discount: number;
  product_price: number;
  created_date: string;
}
