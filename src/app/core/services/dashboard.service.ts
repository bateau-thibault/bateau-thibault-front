import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StockProduitApi } from './products.service';

@Injectable({
  providedIn: 'root',
})
export class StatsService {
  constructor(private http: HttpClient) {}
  private serverUrl = 'http://127.0.0.1:8000/';

  getStatsFromJson(): Observable<StockProduitApi[]> {
    const access_token = localStorage.getItem('access_token');

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${access_token}`,
    });

    return this.http
      .get<any[]>(this.serverUrl + 'transactions', { headers });
  }
  
}
