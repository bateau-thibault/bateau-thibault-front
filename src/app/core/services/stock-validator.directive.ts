import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appStockValidator]'
})
export class StockValidatorDirective {
  private regex: RegExp = new RegExp(/[.,]/);

  constructor(private renderer: Renderer2) {}

  @HostListener('input', ['$event']) onInput(event: Event): void {
    const inputElement = event.target as HTMLInputElement;
    const stockValue = inputElement.value;

    if (this.regex.test(stockValue)) {

      this.renderer.setStyle(inputElement, 'background-color', 'red');
    } else {
      
      this.renderer.removeStyle(inputElement, 'background-color');
    }
  }
}
