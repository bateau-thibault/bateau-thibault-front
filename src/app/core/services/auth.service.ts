import { Observable, catchError, map, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { jwtDecode } from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router, private http: HttpClient) { }

  async checkIfTokenValid(): Promise<boolean> {
    const accessToken = localStorage.getItem('access_token');
    if (!accessToken) {
      this.router.navigate(['/login']);
      return false;
    }
    
    const tokenExpired = this.isTokenExpired(accessToken);

    if (tokenExpired) {
      const refreshSuccessful = await this.refreshToken();
      if (refreshSuccessful) {
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      }
    } else {
      return true;
    }
  }

  private isTokenExpired(token: string): boolean {
    try {
      const decodedToken: any = jwtDecode(token);
      const now = Date.now() / 1000; 
  
      return decodedToken.exp < now;
    } catch (error) {
      return true;
    }
  }

  async refreshToken(): Promise<boolean> {
    localStorage.removeItem('access_token');
    const refresh_token = localStorage.getItem('refresh_token');
    const body = { refresh: refresh_token };
    try {
      const response: any = await this.http.post('http://127.0.0.1:8000/api/token/refresh/', body).toPromise();
      localStorage.setItem('access_token', response.access);
      return true;
    } catch (error) {
      localStorage.removeItem('refresh_token');
      return false;
    }
  }
}
