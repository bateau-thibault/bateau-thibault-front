import { Component, OnInit, Input } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import {
  Product,
  ProductsService,
} from 'src/app/core/services/products.service';

@Component({
  selector: 'app-details-produits',
  templateUrl: './details-produits.component.html',
  styleUrls: ['./details-produits.component.css'],
})
export class DetailsProduitsComponent implements OnInit {
  listeProduits: Product[] = [];
  poissons: Product[] = [];
  fruitsDeMer: Product[] = [];
  crustaces: Product[] = [];

  produit = {
    stock: '',
  };

  constructor(private productsService: ProductsService, private authService: AuthService) {}

  async ngOnInit() {
    var isTrue = await this.authService.checkIfTokenValid();

    if(isTrue)
    {
      await this.getProducts();
    }
  }

  async getProducts() {
    try {
      const res: Product[] | undefined = await this.productsService
        .getProductsFromJson()
        .toPromise();
      if (res !== undefined) {
        this.listeProduits = res;
      } else {
        console.error('Les données JSON sont undefined.');
      }
    } catch (err) {
      console.error('Erreur lors du chargement des données JSON :', err);
    }
  }

  getProductsByCategory(categoryIndex: number): Product[] {
    switch (categoryIndex) {
      case 0:
        return this.listeProduits.filter((produit) => produit.category === 0);
      case 1:
        return this.listeProduits.filter((produit) => produit.category === 1);
      case 2:
        return this.listeProduits.filter((produit) => produit.category === 2);
      default:
        return [];
    }
  }

  modifierStockAndPurchasePrice(produit: Product) {
    if (produit.modifiedStock !== undefined && produit.achat_prix != null) {
      if (produit.modifiedStock + produit.quantityInStock < 0) {
        produit.quantityInStock = 0;
      } else {
        produit.quantityInStock =
          produit.modifiedStock + produit.quantityInStock;
      }
      this.productsService.updateProductStockAndPurchasePrice(produit);
    }
  }

  IsNan(number: Number) {
    return Number.isNaN(number);
  }

  modifierPourcentage(produit: Product) {
    if (produit.modifiePourcentage !== undefined) {
      produit.discount = produit.modifiePourcentage;

      this.productsService.updatedPourcentageProduct(produit).subscribe(
        (updatedPourcentage) => {
          // Gérez la réponse si nécessaire
          console.log(
            'Pourcentage mis à jour avec succès :',
            updatedPourcentage
          );
        },
        (error) => {
          console.log('produit ', produit);
          console.error('Erreur lors de la mise à jour du porcentage :', error);
        }
      );
    }
  }

  modifierStockList(produits: Product[]) {
    const observables: Observable<Product>[] = [];

    produits.forEach((produit) => {
      if (produit.modifiedStock !== undefined) {
        if (produit.modifiedStock + produit.quantityInStock < 0) {
          produit.quantityInStock = 0;
        } else {
          produit.quantityInStock =
            produit.modifiedStock + produit.quantityInStock;
        }
        const stockUpdateObservable =
          this.productsService.updateProductStockList(produit);

        observables.push(stockUpdateObservable);
      }
    });

    // forkJoin pour envoyer toutes les mises à jour de stock en même temps
    if (observables.length > 0) {
      forkJoin(observables).subscribe(
        (updatedProducts) => {
          // Géestion erreur
          console.log('Stocks mis à jour avec succès :', updatedProducts);
        },
        (error) => {
          console.error('Erreur lors de la mise à jour des stocks :', error);
        }
      );
    }
  }
}
